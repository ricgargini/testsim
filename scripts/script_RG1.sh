conda install fastqc
conda install cutadapt
conda install star
conda install multiqc


mkdir -p res/genome
 
wget -O res/genome/ecoli.fasta.gz ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz 
gunzip -k res/genome/ecoli.fasta.gz

mkdir -p res/genome/star_index
STAR --runThreadN 4 --runMode genomeGenerate --genomeDir res/genome/star_index/ --genomeFastaFiles res/genome/ecoli.fasta --genomeSAindexNbases 9

cd /home/vant/testsim2
bash script_fastcutSTAR.sh

multiqc -o out/multiqc .
